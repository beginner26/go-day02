package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Masukan kata: ")
	scanner.Scan()
	text := scanner.Text()

	m := map[rune]int{}
	for _, char := range text {
		m[char] += 1
	}
	chars := []rune{}
	for char := range m {
		chars = append(chars, char)
	}
	sort.Slice(chars, func(i, j int) bool {
		return m[chars[i]] < m[chars[j]]
	})
	for _, char := range chars {
		fmt.Printf("%c = %d\n", char, m[char])
	}
}
