package main

import "fmt"

func main2() {
	for i := 1; i < 2; i++ {
		for j := 1; j < 10; j++ {
			fmt.Printf("%d + %d = %d \n", i, j, i+j)
		}
	}
	fmt.Println(" ")
	fmt.Println("===========")

	var n int = 5
	for i := 0; i <= n; i++ {
		// untuk print spasi
		for j := 0; j < n-i; j++ {
			fmt.Printf(" ")
		}
		// untuk print bintang
		for k := 0; k < i; k++ {
			fmt.Printf("*")
		}
		// setiap perulangan i enter
		fmt.Println()
	}
	fmt.Printf("mencetak %d baris bintang", n)
}
