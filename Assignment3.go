package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main3() {
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Hitung Luas Segitiga")
		fmt.Println("2. Hitung Luas Lingkaran")
		fmt.Println("3. Hitung Luas Persegi")
		fmt.Println("4. Exit")

		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}

		switch userInput {
		case 1:
			fmt.Println("Menghitung luas Segitiga")

			fmt.Printf("Masukan alas: ")
			scanner.Scan()
			a, _ := strconv.ParseFloat(scanner.Text(), 10)

			fmt.Printf("Masukan tinggi: ")
			scanner.Scan()
			t, _ := strconv.ParseFloat(scanner.Text(), 10)

			var luasSegitiga float64 = 0.5 * a * t
			fmt.Printf("Luas segitiga adalah %.2f", luasSegitiga)
		case 2:
			fmt.Println("Menghitung luas Lingkaran")

			fmt.Printf("Masukan phi: ")
			scanner.Scan()
			phi, _ := strconv.ParseFloat(scanner.Text(), 10)

			fmt.Printf("Masukan jari-jari: ")
			scanner.Scan()
			r, _ := strconv.ParseFloat(scanner.Text(), 10)

			var luasLingkaran float64 = phi * r * r
			fmt.Printf("Luas lingkaran adalah %.2f", luasLingkaran)
		case 3:
			fmt.Println("Menghitung luas Persegi")

			fmt.Printf("Masukan panjang: ")
			scanner.Scan()
			p, _ := strconv.ParseFloat(scanner.Text(), 10)

			fmt.Printf("Masukan lebar: ")
			scanner.Scan()
			l, _ := strconv.ParseFloat(scanner.Text(), 10)

			var luasPersegi float64 = p * l
			fmt.Printf("Luas persegi adalah %.2f", luasPersegi)
		}
	}

}
